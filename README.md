Required Project: HTML & CSS
Goal:
To create a 2 page website that utilizes aspects of HTML and CSS content taught in Foundations.

Learning Objectives:
The student will learn how to use HTML tags and CSS styling to create a personal profile page and a gallery page.

Instructions:
Alright, so let’s put it all together! Most of us use Facebook regularly but if you do not, here is a link to their site. Two of the main features of Facebook are: profile creation and photo galleries. Let’s try to create two simple web pages that accomplish the same goal. Start with the following:

Create a folder on your Desktop named Foundations-Project-Your-Full-Name
 Create three files within this folder named as follows:
 profile.html
gallery.html
main.css
Each file will represent a different page on your fictitious website. The profile.html will represent your profile page while the gallery.html file will represent your gallery page. On your profile page, try adding the following:

A title using an h1
 A navigation bar with 3 links:
 Home -> profile.html
Profile -> profile.html
Gallery -> gallery.html
 A picture of yourself
 A unordered list where you identify your:
 Birthday
Hobbies
Favorite TV Shows
 Two short paragraphs describing yourself
 A footer area with links to social media accounts that you have
 Twitter -> twitter profile
Facebook -> facebook profile
On your gallery page, try adding the following:

A title using an h1
 A navigation bar with 3 links:
 Home -> profile.html
Profile -> profile.html
Gallery -> gallery.html
 6 photos arranged in a grid format
 A footer area with links to social media accounts that you have
 Twitter -> twitter profile
Facebook -> facebook profile
In your stylesheet, try doing the following:

Have the stylesheet linked to both the profile.html and gallery.html page
 Style the following elements:
 All titles
The navigation bar
The footer
The photos in the gallery page
The goal is not to have it be perfect but to have you try out different color combinations, fonts, sizing of text and placement of items on the page. The information on the profile itself does not have to be accurate. So be as creative as possible with the content and the design.

Completion Requirements:
The completed assignment should:

have a title of on each page
have a navigation on each page
have a stylesheet that corresponds to both pages
have styled content on both pages
have a footer on each page
2 paragraphs of bio content on the profile page
have a profile image on the profile page
have a more info section listing interests, birthday and favorite tv shows
have 6 images in their gallery page
Submission Requirements:
Create a zip file with your html, css, and assets. Upload this zip file on the following screen.
req.txt
Displaying req.txt.
